"""
This file contains a class that analyzes agriculture data
from the Agricultural total factor productivity (USDA) dataset.

"""

# %load Class/agros_class.py
import os
import warnings
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
import geopandas as gpd
from pmdarima import auto_arima


class AgrosAnalysis:
    """
    A class for analyzing agriculture data from the Agricultural
    total factor productivity (USDA) dataset.

    Attributes
    ---------------
    url: str
        The URL of the crop data file.
    file_path: str
        The file path where the crop data file is saved.
    to_df: pandas.DataFrame
        The crop data loaded into a pandas DataFrame.

    """

    # Dictionary to match country names on geo_data and agros data
    merge_dict = {
        "Bosnia and Herz.": "Bosnia and Herzegovina",
        "Central African Rep.": "Central African Republic",
        "Dem. Rep. Congo": "Democratic Republic of Congo",
        "Dominican Rep.": "Dominican Republic",
        "Eq. Guinea": "Equatorial Guinea",
        "S. Sudan": "South Sudan",
        "Solomon Is.": "Solomon Islands",
        "Timor-Leste": "Timor",
        "United States of America": "United States",
    }

    def __init__(self, url: str):
        """
        Initializes a CropAnalysis object.

        Parameters
        ---------------
        url: str
            The URL of the crop data file.

        Raises
        ---------------
        ValueError
            The file must be the Agricultural total factor productivity (USDA) dataset.
        FileNotFoundError
            The user must be in the project's root directory.

        """
        url_string = (
            "https://raw.githubusercontent.com/owid/owid-datasets/master/"
            "datasets/Agricultural%20total%20factor%20productivity%20(USDA)"
            "/Agricultural%20total%20factor%20productivity%20(USDA).csv"
        )

        # The file must be the Agricultural total factor productivity (USDA) dataset
        if url != url_string:
            raise ValueError(
                "Wrong URL provided. You must provide the URL from "
                "the Agricultural total factor productivity (USDA) dataset"
            )

        # Must be in the project's root directory
        if os.getcwd().endswith("group_03"):
            self.url = url
            self.file_path = os.path.join(
                os.getcwd(), "downloads", "agriculture_data.csv"
            )
            self.to_df = None
        else:
            raise FileNotFoundError("You are not in the project's root directory")

    def download(self) -> None:
        """
        Downloads the crop data from the specified URL and saves it to a file.

        If the file already exists, the data is loaded from the file
        instead of downloading it again.

        Either way, the method creates a DataFrame using the "to_df" attribute.
        The DataFrame is cleaned to remove regions and only contain countries.

        """

        regions = [
            "Asia",
            "Caribbean",
            "Central Africa",
            "Central America",
            "Central Asia",
            "Central Europe",
            "Developed Asia",
            "Developed countries",
            "East Africa",
            "Eastern Europe",
            "Europe",
            "High income",
            "Horn of Africa",
            "Latin America and the Caribbean",
            "Least developed countries",
            "Low income",
            "Lower-middle income",
            "North Africa",
            "North America",
            "Northeast Asia",
            "Northern Europe",
            "Oceania",
            "Pacific",
            "South Asia",
            "Southeast Asia",
            "Southern Africa",
            "Southern Europe",
            "Sub-Saharan Africa",
            "Sahel",
            "Upper-middle income",
            "West Africa",
            "West Asia",
            "Western Europe",
            "World",
        ]

        if os.path.exists(self.file_path):
            data_frame = pd.read_csv(self.file_path)

        else:
            data_frame = pd.read_csv(self.file_path)
            os.makedirs(os.path.dirname(self.file_path), exist_ok=True)
            self.to_df.to_csv(self.file_path, index=False)

        # Removing regions from entity columns
        data_frame = data_frame[~data_frame["Entity"].isin(regions)]
        # Reading the geodata and renaming the countries
        geo_data = gpd.read_file(gpd.datasets.get_path("naturalearth_lowres")).replace(
            {"name": self.merge_dict}
        )
        # Merging the two datasets
        self.to_df = geo_data.merge(
            data_frame, how="inner", left_on="name", right_on="Entity"
        )

    def countries(self) -> list[str]:
        """
        Returns a list of unique countries in the data frame.

        Returns
        ---------------
        countries_list: list[string]
            The list of unique countries in the data frame.

        """
        countries_list = self.to_df["Entity"].unique().tolist()
        return countries_list

    def corr_matrix(self) -> sns.matrix.ClusterGrid:
        """
        Plots a heatmap correlation of the "_quantity" columns.

        Returns
        ---------------
        corr_plot: sns.matrix.ClusterGrid
            The heatmap correlation of the "_quantity" columns.

        """
        quantity_df = self.to_df.loc[:, self.to_df.columns.str.contains("_quantity")]

        # Correlation between different variables
        corr = quantity_df.corr()
        # Set up the matplotlib plot configuration
        plt.subplots(figsize=(12, 10))
        # Generate a mask for upper triangle
        mask = np.triu(np.ones_like(corr, dtype=bool))
        # Configure a custom diverging colormap
        cmap = sns.diverging_palette(230, 20, as_cmap=True)
        # Plot the heatmap
        corr_plot = sns.heatmap(corr, annot=True, mask=mask, cmap=cmap)
        # Add source to plot
        plt.annotate(
            "Source: Agricultural total factor productivity (USDA)",
            (0, 0),
            (0, -150),
            fontsize=8,
            xycoords="axes fraction",
            textcoords="offset points",
            va="bottom",
        )
        return corr_plot

    def area_chart(self, country: str, normalize: bool):
        """
        Create an area chart for the '_output_' columns for a specific country or all countries.
        This chart can be normalized or not according to the "normalize" parameter.

        Parameters
        ---------------
        country: str
            Represents the country to plot the data for.
            If None or 'World', the data for all countries will be plotted.
        normalize: bool
            Indicates whether to normalize the data.

        Raises
        ---------------
        ValueError
            The given country must exist in the data.

        """

        # Check if the given country exists in the data, or is Null or 'World'
        if (
            country is not None
            and country != "World"
            and country not in self.to_df["Entity"].unique()
        ):
            raise ValueError(f"Country {country} does not exist in the data.")

        # Subset the data to the selected country or all countries
        if country is None or country == "World":
            data = (
                self.to_df.groupby("Year")
                .sum()[
                    [
                        "fish_output_quantity",
                        "crop_output_quantity",
                        "animal_output_quantity",
                        "output_quantity",
                    ]
                ]
                .reset_index()
            )
        else:
            data = self.to_df[self.to_df["Entity"] == country][
                [
                    "Year",
                    "fish_output_quantity",
                    "crop_output_quantity",
                    "animal_output_quantity",
                    "output_quantity",
                ]
            ]

        # Normalize the data if necessary
        if normalize:
            outputs = [
                "fish_output_quantity",
                "crop_output_quantity",
                "animal_output_quantity",
            ]

            for output in outputs:
                data[output] = data[output] / data["output_quantity"] * 100
            title = "(Normalized)"
            # set the index to the year column
            area_df = data.set_index("Year")
        else:
            area_df = data.set_index("Year")
            title = ""

        # Create the area chart
        plt.stackplot(
            data["Year"],
            data["fish_output_quantity"],
            data["crop_output_quantity"],
            data["animal_output_quantity"],
            labels=area_df.columns,
            colors=["Cyan", "Darkgreen", "Red"],
        )
        plt.xlabel("Year")
        plt.ylabel("Output")
        plt.title(
            f'Agriculture Output for {country if country is not None else "World"} {title}'
        )
        plt.legend(loc="upper left")
        plt.annotate(
            "Source: Agricultural total factor productivity (USDA)",
            (0, 0),
            (0, -50),
            fontsize=8,
            xycoords="axes fraction",
            textcoords="offset points",
            va="bottom",
        )
        plt.show()

    def comparison_chart(self, countries: str or list[str]):
        """
        Plot a comparison of the '_output_' columns for the countries provided.
        Creates three plots to compare each country to each type of output.
        Only creates one plot if only one country is provided.

        Parameters
        ---------------
        countries: str or list[str]
            Contains the country/countries to plot the data for.

        Raises
        ---------------
        ValueError
            The given countries must exist in the data.

        """

        # Convert the countries input to a list if it's a string
        if isinstance(countries, str):
            countries = [countries]

        # Check if all of the given countries exist in the data
        if not set(countries).issubset(set(self.to_df["Entity"].unique())):
            invalid_countries = set(countries) - set(self.to_df["Entity"].unique())
            raise ValueError(
                f"The following countries do not exist in the data: {invalid_countries}"
            )

        # Subset the data to the selected countries
        outputs = [
            "fish_output_quantity",
            "crop_output_quantity",
            "animal_output_quantity",
        ]

        # create a scatter plot with size based on 'land_area'
        if len(countries) == 1:
            data = (
                self.to_df[self.to_df["Entity"].isin(countries)]
                .groupby(["Year", "Entity"])
                .sum()[outputs]
                .unstack()
            )
            data.columns = data.columns.droplevel()

            # Plot the comparison chart
            data.plot(kind="line")
            plt.xlabel("Year")
            plt.ylabel("Output")
            plt.title(f'Agriculture Output Comparison for {", ".join(countries)}')
            plt.legend(loc="upper left", labels=outputs)
            plt.annotate(
                "Source: Agricultural total factor productivity (USDA)",
                (0, 0),
                (0, -50),
                fontsize=8,
                xycoords="axes fraction",
                textcoords="offset points",
                va="bottom",
            )
            plt.show()

        else:
            for output in outputs:
                sub_string = output.split("_")
                data = (
                    self.to_df[self.to_df["Entity"].isin(countries)]
                    .groupby(["Year", "Entity"])
                    .sum()[[output]]
                    .unstack()
                )
                data.columns = data.columns.droplevel()

                # Plot the comparison chart
                data.plot(kind="line")
                plt.xlabel("Year")
                plt.ylabel("Output")
                plt.title(
                    f'{sub_string[0].capitalize()} Output Comparison for {", ".join(countries)}'
                )
                plt.legend(loc="upper left")
                plt.annotate(
                    "Source: Agricultural total factor productivity (USDA)",
                    (0, 0),
                    (0, -50),
                    fontsize=8,
                    xycoords="axes fraction",
                    textcoords="offset points",
                    va="bottom",
                )
                plt.show()

    def gapminder(self, year: int):
        """
        Create a scatter plot for fertilizer quantity and output quantity, using the
        "cropland_quantity" variable for the size of the dots. Each dot represents a country.
        Log-scale is being used to help visualization.

        Parameters
        ---------------
        year: int
            The year to plot the data for.

        Raises
        ---------------
        TypeError
            The year argument must be an integer.

        """

        # check that the year argument is an integer
        if not isinstance(year, int):
            raise TypeError("Year argument must be an integer")

        # subset the data to the given year and drop any rows with missing values
        data = self.to_df[self.to_df["Year"] == year].dropna(
            subset=["fertilizer_quantity", "output_quantity"]
        )

        plt.scatter(
            data["fertilizer_quantity"],
            data["output_quantity"],
            s=np.log(data["cropland_quantity"]) * 10,
            alpha=0.4,
            label="Cropland Quantity",
        )
        plt.xscale("log")
        plt.yscale("log")
        plt.xlabel("Fertilizer Quantity")
        plt.ylabel("Output Quantity")
        plt.legend(labels=["Cropland Quantity"])
        plt.title(f"Agriculture Scatter Plot for {year}")
        plt.annotate(
            "Source: Agricultural total factor productivity (USDA)",
            (0, 0),
            (0, -50),
            fontsize=8,
            xycoords="axes fraction",
            textcoords="offset points",
            va="bottom",
        )
        plt.show()

    def choropleth(self, year: int):
        """
        Creates a choropleth plot for the TFP (total factor productivity).

        Parameters
        ---------------
        year: int
            The year to plot the data for.

        Raises
        ---------------
        TypeError
            The year argument must be an integer.

        """

        # check that the year argument is an integer
        if not isinstance(year, int):
            raise TypeError("Year argument must be an integer")

        # subset the data to the given year and drop any rows with missing values
        data = self.to_df[self.to_df["Year"] == year]

        # Plot the choropleth
        a_x = plt.subplots(figsize=(10, 6))[1]
        data.plot(cmap="Greens", column="tfp", linewidth=0.8, ax=a_x, edgecolor="0.6")
        a_x.axis("off")
        a_x.set_title("Choropleth Map")

        # Add a colorbar
        vmin = data["tfp"].min()
        vmax = data["tfp"].max()
        s_m = plt.cm.ScalarMappable(
            cmap="Greens", norm=plt.Normalize(vmin=vmin, vmax=vmax)
        )
        s_m.set_array([])
        cbar = plt.colorbar(s_m, shrink=0.5, alpha=0.8)
        cbar.ax.set_title("TFP")
        plt.show()

    def predictor(self, countries: list[str]):
        """
        Creates a line plot with the past and predicted values until 2050 for each of the
        selected countries using ARIMA for the prediction.

        Parameters
        ---------------
        countries: list[str]
            A list of up to 3 countries.

        Raises
        ---------------
        ValueError
            The input list must have a maximum length of 3.
        TypeError
            The input type must be a list.
        ValueError
            At least one of the countries must have a valid name.


        """

        if len(countries) > 3:
            raise ValueError("The input list must have a maximum length of 3")

        if not isinstance(countries, list):
            raise TypeError("The input type must be a list.")

        invalid_countries = set(countries) - set(self.to_df["Entity"].unique())
        valid_countries = list(set(countries) - invalid_countries)

        if len(invalid_countries) == 3:
            raise ValueError(
                f"The following countries do not exist in the data:\n {invalid_countries}\n"
                f"\nThese are the available countries:\n {self.countries()}"
            )

        # Ignore the warnings, as some parameters may be incompatible with some ARIMA settings
        warnings.filterwarnings("ignore")

        # Color list
        colors = ["Green", "Blue", "Orange"]
        i = 0

        while i < len(valid_countries):
            for country in valid_countries:
                color = colors[i]

                # Dataframe with past data for the country
                past_df = self.to_df[self.to_df["Entity"] == country][["tfp"]]
                past_df.head()
                past_df["Time"] = pd.date_range(
                    start=str(self.to_df[["Year"]].min()[0]) + "-01-01",
                    periods=self.to_df[["Year"]].max()[0]
                    - (self.to_df[["Year"]].min()[0])
                    + 1,
                    freq="Y",
                )

                stepwise_fit = auto_arima(
                    self.to_df[self.to_df["Entity"] == country][["tfp"]],
                    start_p=1,
                    start_q=1,
                    max_p=3,
                    max_q=3,
                    m=1,
                    start_P=0,
                    seasonal=False,
                    d=None,
                    D=1,
                    error_action="ignore",  # Ignore incompatible settings
                    suppress_warnings=True,
                    stepwise=True,
                )

                # Dataframe with prediction data
                prediction = pd.DataFrame(
                    stepwise_fit.predict(
                        n_periods=2050 - int(self.to_df[["Year"]].max()[0] + 1)
                    )
                )  # , index=dair.index)
                prediction["Time"] = pd.date_range(
                    start=str(self.to_df[["Year"]].max()[0] + 1) + "-01-01",
                    periods=2050 - (self.to_df[["Year"]].max()[0] + 1),
                    freq="Y",
                )
                # Plots
                plt.plot(past_df["Time"], past_df["tfp"], c=color, label=country)
                plt.plot(
                    prediction["Time"],
                    prediction.iloc[:, 0],
                    linestyle="dotted",
                    c=color,
                    label=f"{country} predicted",
                )
                plt.xlabel("Year")
                plt.ylabel("TFP")
                plt.legend(loc="upper left")
                plt.title("Past and predicted TFP by country")
                plt.annotate(
                    "Source: Agricultural total factor productivity (USDA)",
                    (0, 0),
                    (0, -50),
                    fontsize=8,
                    xycoords="axes fraction",
                    textcoords="offset points",
                    va="bottom",
                )

                # Iterate through color list
                i += 1
