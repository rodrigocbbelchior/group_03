.. Agricultural Productivity Analysis documentation master file, created by
   sphinx-quickstart on Thu Mar  9 16:45:01 2023.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Agricultural Productivity Analysis's documentation!
==============================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   modules


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
