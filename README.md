# Group_03

## Agriculture Productivity Analysis


## Description
This project's goal is to analyze agricultural data from the Agricultural total factor productivity (USDA) using the AgroAnalysis class.

The class AgroAnalysis provides several methods for analysis of this data as demonstrated in the Showcase_Group_03.ipynb file under the project's root directory.

These different methods allow the user to download the data, list all the countries in the dataset, plot a correlation matrix of the variables,
plot an area chart for the '_output_' columns for a specific country or all countries, a comparison chart of a list of countries, a gapminder visualization, 
a choropleth of the total factor productivity and a prediction of future values of tfp for a list of countries.

## Usage
To make use of this project there are some initial steps that must be taken into consideration:
1. Clone the git repository in a desired directory.

2. Create the conda environment using the following command: conda env create -f environment.yml

3. Activate the environment: conda activate AdPro

To initialize an instance of the AgrosAnalysis class make sure you are in the root directory of this project (group_03).

The Showcase_Group_03.ipynb file can be run in any Jupyter Notebook environment that supports Python 3.x.


## Support
For any issues regarding this project access the group_03/docs/_build/html/index.html file and open it in a browser for detailed documentation.

Further support is not currently available.

## Authors and acknowledgment
Rodrigo Belchior https://gitlab.com/rodrigocbbelchior

Bernardo Carrasqueiro https://gitlab.com/bernardocarrasqueiro

Francisco Nobre https://gitlab.com/francisconobre

António da Cunha https://gitlab.com/antoniodacunha

## License
This project uses the GNU GENERAL PUBLIC LICENSE V 3.0.

## Project status
This project is complete, no further developments should be expected.
